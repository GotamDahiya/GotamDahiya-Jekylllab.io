---
layout: post
title: "Helper Scripts"
literal_block: |
    This project contains scripts for data collection, pre-processing and .cleaning of images. For downloading Flickr API is used, while for the cleaning, and pre-processing Pillow, OS, Pandas and Numpy Python libraries are used.
link_title: Helper Scripts
link_project: https://gitlab.com/GotamDahiya/helper-scripts
date: 2020-09-30 22:16:47
---

## Table of Contents

* [Introduction](#introduction)
* [Scripts](#scripts)
    * [Renaming files in a Directory](#rename_file_dir)
    * [Split Images into Class Folders](#split_labels_dir)
    * [Split Images into Classes in CSV](#split_labels_csv)
    * [Preparing Training, Testing and Validation CSV Files](#prepare_train_test_valid_csv)
    * [Preparing Training, Testing and Validation folders](#prepare_train_test_valid_folder)
    * [Downloading Images from Flickr](#download_images_flickr)
    * [Downloading Images from Unsplash](#download_images_unsplash)
    * [List of Images to Delete](#delete_images_list)
    * [Parsing Images Stored in a CSV file](#parse_images_from_csv)
    * [Convert TIF to Images](#convert_tif_image)

## Introduction <a name="introduction"></a>

This project contains scripts for automating the process of collecting data, cleaning and finally processing it. 

Currently it allows for renaming of files in a directory, splitting into training,testing and validation CSV files, splitting of images into respective class folders based.

## Scripts <a name="scripts"></a>

***Update*** All pickle statements are updated to JSON statements for interoperability between programming languages.

# Renaming files in a Directory <a name="rename_file_dir"></a>

Link: [rename_files_dir.py](https://gitlab.com/GotamDahiya/helper-scripts/-/blob/master/rename_files_dir.py)

Renames all the images present in a directory following this set convention, **class-label_file-number**. File number is updated using a counter.

This comes in handy after downloading or removing images from a set folder. Allows for easier understanding of the dataset.

# Split Images into Class Folders <a name="split_labels_dir"></a>

Link: [split_labels_dir.py](https://gitlab.com/GotamDahiya/helper-scripts/-/blob/master/split_labels_dir.py)

Splits images into respective class folders under the dataset directory. It either splits them based upon the class name being present in the original filename or the binarized class name.

A JSON file containing the binarized class names is stored along with the directory for further use and maintaining the binarizer obtained through this script.

# Split Images into Classes in CSV <a name="split_labels_csv"></a>

Link: [split_labels_csv.py](https://gitlab.com/GotamDahiya/helper-scripts/-/blob/master/split_labels_csv.py)

Similar in function to the one above. It differs in the aspect that it does not split the original dataset into directories, instead a *CSV* file is made where each image is alloted its class names and corresponding binarized class name.

This can be used when the dataset is massive and duplicating it in another directory will take too much space and memory. The CSV file allows for splitting of dataset without consuming space and memory.

# Preparing the Training, Testing and Validation CSV files <a name="prepare_train_test_valid_csv"></a>

Link: [prepare_train_test_valid_csv](https://gitlab.com/GotamDahiya/helper-scripts/-/blob/master/prepare_train_test_valid_csv.py)

This program splits the dataset when present in class [folders](#split_labels_dir) or [*CSV*](#split_labels_csv) into the training, validation and testing in the ratio of 7:2:1. 

The final split is stored in a separate CSV file when using class folders or updated in the same file when using the CSV file.

The features stored are the filename, class name, binarized class name and which split the particular image it belongs too.

# Preparing Training, Testing and Validation folders <a name="prepare_train_test_valid_folder"></a> 
Link: [prepare_train_test_valid_folder](https://gitlab.com/GotamDahiya/helper-scripts/-/blob/master/prepare_train_test_valid_folder.py)

This program creates training, validation and testing folders based upon the CSV files produced by the above program. Each split contains the classes of the images as sub-folders.

# Downloading images from Flickr <a name="download_images_flickr"></a>

Link: [download_images_flickr.py](https://gitlab.com/GotamDahiya/helper-scripts/-/blob/master/download_images_flickr.py)

This script is used for downloading images from *Flickr* using its API. Per page a maximum of 100 images are downloaded. This is done as one request can only download 500 images from *Flickr*. The specified number of downloads to be made are split into groups of 100. With each request complete the page number is incremented by one which drastically reduces the number of duplicates downloaded.

The filenames of the images downloaded correspond to the image's flickr ID. This results in duplicates only updating themselves in the previously downloaded file if it exists.

[Rename Script](#rename_file_dir) can be called after this script finishes running so as to rename the files following the set convention.

# Downloading images from Unsplash <a name="download_images_unsplash"></a>

Link: [download_images_unsplash](https://gitlab.com/GotamDahiya/helper-scripts/-/blob/master/download_images_unsplash.py)

It is pratically the same program as before but downloads images from *Unsplash* rather than from *Flickr*. The images present in *Unsplash* are of a higher quality and far better than that of *Flickr*.

# List of Images to Delete <a name="delete_images_list"></a>

Link: [delete_images_list.py](https://gitlab.com/GotamDahiya/helper-scripts/-/blob/master/delete_list_images.py)

Used for creating a list of images to be deleted from a directory which is not relevant to the class. It displays each image present in the directory sequentially. The display of images is controlled using *a* and *d* keys with *a* being for going one image back and *d* for one image forward. The *q* is used for exiting the process while the *w* is used for image filename to the list of images to be deleted. *r* is used for removing the filename from the list. 

A window is displayed beside the image to show the filename of the current image.

# Parsing Images stored in a CSV file <a name="parse_images_from_csv"></a>

Images stored in a CSV file are extracted, converted into separate files which are stored in the corresponding class folders. Currently not tested.

# Converting TIF files to Images <a name=convert_tif_image"></a>

Link: [convert_tif_image](https://gitlab.com/GotamDahiya/helper-scripts/-/blob/master/convert_tif_image.py)

This script is used for extracting the Red, Green and Blue bands from the TIF files, stacking them together and saving the resultant array as an image. 

*Rasterio* is used for extracting the RGB bands.