---
layout: post
title: "Image Search Engine"
literal_block: |
    A specialised Image Search Engine is being developed which will a user to search for images based upon a query image.
    
    The aim is that very similar images be returned based upon the features of the query image rather than objects. 

    CNN for extracting features, Autoencoders for dimensionality reduction, Clustering for grouping images with very similar features and finally Euclidean Distance for comparing the features with respect to the query image. The final step is display of images.
link_title: ImageSearchEngine
link_project: https://gitlab.com/GotamDahiya/ImageSearchEngine
date: 2020-09-29 22:16:47
---

## Table of Contents

* [Introduction](#introduction)
* [Description](#description)
    * [Convolutional Neural Network](#convolutional-neural-network)
    * [Autoencoders](#autoencoders)
    * [Clustering](#clustering)
    * [Euclidean Distance](#euclidean-distance)
    * [Siamese Network](#siamese-networks)

## Introduction <a name="introduction"></a>

This Image Search Engine is designed for both general use as well as specialised datasets. This is bought about by the use of *clustering* and *Siamese Networks*.

The project uses Convolutional Neural Networks for extracting features from images, Autoencoders for dimensionality reduction, Clustering to group them together based upon the relevant features and finally Euclidean Distance to rank the resultant images with respect to the query image. 

## Description <a name="description"></a>

# Convolutional Neural Network <a name="convolutional-neural-network"></a>

Images are first fed through a Convolutional Neural Network for extracting the various features of an image. Currently, *MobileNet* a CNN mode developed by Andrew G. Howard, Menglong Zhu, et. al. at Google Inc. Here is a link to their original [paper](https://arxiv.org/pdf/1704.04861.pdf).

MobileNetV2 is chosen as the number of trainable parameters is quite lower than VGG16 and ResNet50 while still maintaining comparable accuracy. The reduced number of trainable parameters reduced feature extraction time and is less compute intensive as compared to the other too. A data generator is used to read the features in batches so as to reduce *Memory* usage.

| ![MobileNet Body Architecture](https://gitlab.com/GotamDahiya/imagesearchengine/-/raw/version2/output/CNN-feature-extraction/V1/MobileNetV2-Architecture.png) |
|:--:|
| ***MobileNetV2 Architecture*** |

# Autoencoders <a name="autoencoders"></a>

The features extracted from images using the CNN are fed into Autoencoders which comprises of two models, *encoder* and *decoder*. The *encoder* is used for dimensionality reduction and extracting the *relevant features*. It is primarily used for compression.

*Decoder* is used for building upon the relevant features and obtain the original features of the images. It is primarily used for decompressing the output of the *encoder* model. More about autoencoders can be found at this [tutorial](https://blog.keras.io/building-autoencoders-in-keras.html) by Francois Chollet.

| ![Encoder Architecture](https://gitlab.com/GotamDahiya/imagesearchengine/-/raw/version2/output/Autoencoders/V2/encoder_plot.png) |
|:--:|
| ***Encoder Architecture*** |

| ![Decoder Architecture](https://gitlab.com/GotamDahiya/imagesearchengine/-/raw/version2/output/Autoencoders/V2/decoder_plot.png) |
|:--:|
| ***Decoder Architecture*** |

| ![Autoencoder Architecture](https://gitlab.com/GotamDahiya/imagesearchengine/-/raw/version2/output/Autoencoders/V2/autoencoder_plot.png) |
|:--:|
| ***Autoencoder Architecture*** |

# Clustering <a name="clustering"></a>

The features after being passed through the *encoder* model are clustered based upon the similarity of their relevant features. *MiniBatch K-Means* is used for clustering the relevant features as passing all of them into memory at once may result in memory overflow, thereby killing the process.

The clusters are planned to be routinely updated using the query images and any new dataset which is made available to the search engine. For the query images, after about 100 or so, while for the new dataset immediately.

This is done so as to obtain a subset of the entire dataset to compare to the query image.

# Euclidean Distance <a name="euclidean-distance"></a>

Euclidean distance is being used to rank the images based upon their feature vectors. Here the distance is measured as the  square root of the summation of the squares of the difference between the feature vector's individual points.

Euclidean Distance gives the absolute distance between any two vectors. Here it is being ussed to measure the similarity between the images. Closer the distance is to 0 the more similar the images are. 

Relevant features extracted from the *Encoder* model are compared using the Euclidean Distance instead of the images themselves as computation time will shoot up.

Siamese Networks(SN) for comparing had to be dropped as the time taken to train the SN model for every new query took too much time and similar results were being given with euclidean distance which is currently faster.

| ![Euclidean Distance](https://i.stack.imgur.com/iWe4J.png) |
|:--:|
| *Euclidean Distance* |


# Siamese Networks <a name="siamese-networks"></a>

Siamese networks are one shot classifiers. They are used for obtaining similarity scores between two images, or in this case the relevant features present in the cluster. Siames Networks or Siamese Neural Networks(SNN) share the same fully convolutioal model as well as the weights since they have to be the exactly the same.

| ![Siamese Network model](./output/SiameseNetwork/seq_model_plot.png) |
|:--:|
| *Siamese Sequential Model* |

| ![Complete Model](./output/SiameseNetwork/sm_model_plot.png) |
|:--:|
| *Comparison Network* |